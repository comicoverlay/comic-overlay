// https://web-ace.jp/youngaceup/contents/1000137/episode/4244/?1=sVCZ8BA&2=qDPLEW2&3=3zJIryI&4=aBp5Eno&5=zRbXFYy&6=ioEcHB9&7=PujEu44&8=faAn3k4&9=tMS6Oaf&10=2OFyQDe&11=NWOaUqY&12=tdBKAme&13=8QGPaxk&14=Fb7Mmvr&15=mZSlJaC&16=6U5wiz0

function update_canvases () {
    var imgViews = Array.from(document.getElementsByClassName("viewerFixedImage"));
    for (i = 0; i < imgViews.length; i++) {
        spacer = imgViews[i].previousElementSibling;
        imgHash = new URL(document.URL).searchParams.get("" + (i + 1));
        if (imgHash != null) {
            spacer.src = "https://i.imgur.com/" + imgHash + ".png";

            // Fixes a bug in the spacer image that causes it to not expand
            // across the full image when the browser goes from Mobile to
            // Desktop mode.
            spacer.removeAttribute("style");
        }
    }
}

document.body.onclick = update_canvases;
document.addEventListener('DOMNodeInserted', update_canvases);
document.body.click()
