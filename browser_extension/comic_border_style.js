// https://comicborder.com/episode/10834108156644502252?0=BQ09wbB&1=OVlAGUa&2=oAqx0R0&3=ffcLFQz&4=ex86nJZ&5=SJMJEJC&6=CxuXFKY&7=H8bMvOR&8=BFnLfzj&9=7qfjKAB&10=7XDNhoj&11=SA2AmaU&12=JOuLBoo&13=fSyrpRG&14=uzpWm2m&15=eKowe04&16=7cpOgrq&17=9WiLCil
// https://kuragebunch.com/episode/10834108156705952451?0=HfVtDQh&1=XHsf6xY&2=XOKz4cC&3=EpMAd01&4=IvlNCwl&5=PZXcunL

// canvas_id -> seen count. To be safe, a seen count of 2 is required for updating the image.
var seen_count = {};
var seen_canvases = [];

function makeDrawImage(canvas, img) {
    return function drawImage() {
        return canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);
    }
}


function update_canvases () {
    canvases = document.querySelectorAll("canvas");
    for (const canvas of canvases) {
        if (canvas.id == "") {
            var index = -1;
            var iterator = canvas.parentElement;
            while( (iterator = iterator.previousSibling) != null ) {index++;}
            canvas.id = index;
            seen_count[canvas.id] = 0;
            seen_canvases[Object.keys(seen_canvases).length] = canvas;
        }
        seen_count[canvas.id] += 1;
    }
    for (i = 0; i < seen_canvases.length; i++) {
        canvas = seen_canvases[i];
        if (seen_count[canvas.id] <= 2) {
            img = new Image();
            img.onload = makeDrawImage(canvas, img);
            img.src = "https://i.imgur.com/" + new URL(document.URL).searchParams.get(canvas.id) + ".png";
        }
    }
}

// Sometimes, the reader provides images instead of canvases (usually for mobile
// browsers). When this happens, we convert the images back to canvases.
function force_use_canvases() {
    page_areas = document.querySelectorAll("p.page-area");
    for (const page_area of page_areas) {
        if (page_area.id == "") {
            var index = -1;
            var iterator = page_area;
            while( (iterator = iterator.previousSibling) != null ) {index++;}
            page_area.id = index;
        }
        if (page_area.children.length == 1 && page_area.children[0].tagName != "CANVAS") {
            img_element = page_area.children[0];
            img_element.style = 'display:none;';
            page_area.insertAdjacentHTML('beforeend',
            '<canvas width="' + img_element.naturalWidth +
            '" height="' + img_element.naturalHeight +
            '" class="page-image js-page-image"></canvas>');
            page_area.children[1].getContext("2d").drawImage(img_element, 0, 0);
        }
    }
}

function update() {
    force_use_canvases();
    update_canvases();
}

document.body.onclick = update;
document.addEventListener("DOMNodeInserted", update);
// Allows bookmarklets to instantly see the translations.
document.body.click();
