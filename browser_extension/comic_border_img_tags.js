// https://comicborder.com/episode/10834108156644502252?1=BQ09wbB&2=OVlAGUa&3=oAqx0R0&4=ffcLFQz&5=ex86nJZ&6=SJMJEJC&7=CxuXFKY&8=H8bMvOR&9=BFnLfzj&10=7qfjKAB&11=7XDNhoj&12=SA2AmaU&13=JOuLBoo&14=fSyrpRG&15=uzpWm2m&16=eKowe04&17=7cpOgrq&18=9WiLCil

var url = new URL(document.URL);
var seen_canvases = [];
// canvas_id -> seen count. To be safe, a seen count of 2 is required for updating the image.
var seen_count = {};

function makeDrawImage(canvas, img) {
    return function drawImage() {
        return canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);
    }
}

// <img class="overlay_img" src="https://cdn.comicborder.com/images/abj-border.svg?1585297725" style="position:absolute;left:0px;top:0px;" />

function update_canvases () {
    canvases = document.querySelectorAll("canvas");
    for (const canvas of canvases) {
        if (canvas.id == "") {
            var index = 0;
            var iterator = canvas.parentElement;
            while( (iterator = iterator.previousSibling) != null ) {index++;}
            canvas.id = index;
            seen_count[canvas.id] = 0;
            seen_canvases[Object.keys(seen_canvases).length] = canvas;
        }
        seen_count[canvas.id] += 1;
    }
    for (i = 0; i < seen_canvases.length; i++) {
        canvas = seen_canvases[i];
        if (canvas.parentElement.getElementsByClassName("overlay_img").length == 0) {
            overlay_img = document.createElement("img");
            overlay_img.className = canvas.className.match("(hidden )?(.*)")[2] + " overlay_img";
            canvas.parentElement.append(overlay_img);

            canvas.parentElement.parentElement = overlay_img.className.match(/(align-[a-z]*)/)[1];
            overlay_img.style = "position:absolute;" + alignment + ":0px";
            overlay_img.src = "https://i.imgur.com/" + url.searchParams.get(canvas.id) + ".png";
            overlay_img.width = "100%";
            overlay_img.height = "100%";
        }
    }
}

document.onclick = update_canvases;
document.addEventListener('DOMNodeInserted', update_canvases);
