# Setup

This document goes over how do things in Chrome, because chances are you have
that browser, or can install that browser. If you can't use Chrome, you should
Google the equivalent settings.

### Enable Chrome's Developer tools

Have Chrome automatically pretty print your JSON. It will save you a lot of
time. See [this Stack Overflow link](https://stackoverflow.com/a/59254822) for
more info on how to achieve that.
