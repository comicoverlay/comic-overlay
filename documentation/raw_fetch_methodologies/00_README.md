## Files in this directory

The other documents in this directory will tell you what methods are used by
various websites, as well as how to find what method was used.

If you are already comfortable reverse engineering methods of downloading
the comic pages, feel free to skip this document and jump to
[02_setup.md](./02_setup.md). From there, jump to any webpage that suits your
fancy.

Otherwise, read the [01_getting_started.md](./01_getting_started.md) page.
