# Mangadex

Take a look at the a chapter from this website and follow along. Ex:

```raw
https://mangadex.org/chapter/853337/1
```

This one has a few alternatives that can be discovered by doing some Google
searching to find the public APIs for the website. They are mostly the same. For
consistency's sake, we're going to continue to show how to pull these the way
the web reader pulls them.

## How the comic pages are fetched

Open up the Developer Console (`Ctrl/Cmd + Shift + J`) and go to `Network`.
Refresh the page to fill out the data there if necessary. We're looking for some
JSON API calls.

There should be two of them that appear, both begin with `"?id="`. Look for the
one with the ID that matches the chapter number. Using the example above, this
should be:

```raw
https://mangadex.org/api/?id=853337&server=null&type=chapter
```

From here, we can pull up the JSON returned and find the images. There are three
important fields:

```json
{
  "hash": "8a3723c1d00d11accdc675d6c969b1ba",
  "server": "https://mangadex.org/data/",
  "page_array": ["b1.jpg", "b2.jpg", "b3.jpg", "b4.jpg", "b5.jpg", "b6.jpg", "b7.jpg", "b8.jpg"]
}
```

All three of these pieces of data are combined to form the URLs of the images
fetched as:

```raw
server + '/' + hash + '/' page_array[i]
```

## Getting next and previous chapters
The other API call the page makes lists all chapters. Filter by language, then
pick the group that matches the given chapter. If the group does not have that
particular chapter, opt for another group (scanlation often changes hands when
groups move on).
