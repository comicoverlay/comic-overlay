# Comic Border

Take a look at the a chapter from this website and follow along. Ex:

```raw
https://comicborder.com/episode/10834108156644502252
```

## How the comic pages are fetched

When opening up the Developer Console, the first thing you may notice in the
`Elements` tab is the script `episode-json`. Copy and paste its elements into
a JSON formatter [(ex)](https://jsonformatter.curiousconcept.com/) and you
can see that it lists off all pages under `readableProduct.pageStructure.pages`.
Within each entry is a full URL with a specification of the width and height of
the page.

Note that first page is simply a link. The current `ComicBorderReader.solve`
method does not attempt to read this page. However, the page can be found by
reading the `<img>` element under the first div in the image container (more
formally, `document.querySelector("#content > div >
div.image-container.js-viewer-content.is-spread > div:nth-child(1) > div > div >
div > img")`).

The files within the `episode-json` may be under a DRM shuffle. According to the
JS source, if `readableProduct.pageStructrue.choJuGiga == 'baku'`, then a DRM
shuffle is required to make the image human readable (search for `skipDrm` in
the source JS files). Otherwise, the image can be directly fetched.

From here, we can loop through all of the sources to get the resource files used
to create the final images (or the final images themselves). Moving over to the
`Sources` tab, we can see that one of the
`cdn.comicborder.com/js/<hash>chunk.js` files defines a function `solve` as:

```
constructor(e, t) {
    super(e),
    this.DIVIDE_NUM = 4,
    this.MULTIPLE = 8,
    this.width = t.width,
    this.height = t.height,
    this.cell_width = Math.floor(this.width / (this.DIVIDE_NUM * this.MULTIPLE)) * this.MULTIPLE,
    this.cell_height = Math.floor(this.height / (this.DIVIDE_NUM * this.MULTIPLE)) * this.MULTIPLE
}
solve() {
    this.view.drawImage(0, 0, this.width, this.height, 0, 0);
    for (var e = 0; e < this.DIVIDE_NUM * this.DIVIDE_NUM; e++) {
        var t = Math.floor(e / this.DIVIDE_NUM) * this.cell_height
          , n = e % this.DIVIDE_NUM * this.cell_width
          , r = Math.floor(e / this.DIVIDE_NUM)
          , i = e % this.DIVIDE_NUM * this.DIVIDE_NUM + r
          , s = i % this.DIVIDE_NUM * this.cell_width
          , o = Math.floor(i / this.DIVIDE_NUM) * this.cell_height;
        this.view.drawImage(n, t, this.cell_width, this.cell_height, s, o)
    }
    this.view.replaceImage()
}
```

This class contains the algorithm for writing the image data onto the canvases
under the `image-container` in the HTML file, which we can then port into Python
to recreate the final image.

## Alternatives

The website uses a function that intentionally sets the canvases to become
tainted by calling `makeTainted` within the same JS file as the function from
the above section. If we replace that resource or rewrite the JS to prevent it
from being called, we can prevent the `makeTainted` call from happening, which
would allow us to download the images directly off of the canvases.

This approach works well for scripts that run directly in the browser, but does
not work well for the methods in this project.
