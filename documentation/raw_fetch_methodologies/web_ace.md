# Web Ace

Take a look at the a chapter from this website and follow along. Ex:

```raw
https://web-ace.jp/youngaceup/contents/1000137/episode/4262/
```

## How the comic pages are fetched

This website does not use any form of shuffling or image modification to display
the correct pages.

When we look at the `Elements` tab and inspect the element containing the comic
pages here (`Ctrl/Cmd + Shift + C`), we can see that the image source contains a 
[UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier), which makes
it difficult for us to iterate through the image URLs from one to the next. We
will need to find another way to find all the image URLs.

If we go to the `Network` tab in the Developer Console, we can refresh the page
and look through the `Initiator` sub-tab on one of the comic pages. From here,
we can see the call stack has functions coming from 
`imageViewerVerticalScroll.js` just before the jquery call to fetch the file.

The `Headers` subtab shows us what URL was requested for the image, but we want
to know how the website knew where to pull the URL from. Peaking at the code in
`imageViewerVerticalScroll.js`, we can trace the image request back to the
source of the image's URL. Clicking on first `imageViewerVerticalScroll.js`
function link in the `Initiator` sub-tab's call stack, we can see:

```javascript
105|    img.attr('src',url );
        // This line of code is where we fetch the file, but we need to know
        // where URL got its value.
...
 82|    var url = URLS[i];
        // The value came from the URLS array. We can now search for how that
        // array got it's value using `Ctrl + F` "URLS ="
...
 29|    $.getJSON(IMAGE_LIST_URL,function(data){
 30|            URLS = data;
...
 11|    IMAGE_LIST_URL = IMAGE_CONTAINER.data("url");//画像JSON配列を取得するURL
...
  7|    var IMAGE_CONTAINER = this;
```

So, the `IMAGE_CONTAINER.data("url")` is an instance of some object that holds
a data field. Looking at the `Elements` tab and searching for `data`, we can
see the following element:

```html
<div class="lazy-container" data-url="/youngaceup/contents/1000137/episode/4262/json/">
```

Note that this URL is relative to the top-level domain. Going to that website
yields a JSON list of all URLs containing the images.

From here, simply do an HTTP request for the image and save it.

## Alternatives

We can skip all the work we did to find the URL source if we make assumptions
about what files to download. Taking a look at the `Elements` tab in the
Developer Console, we can see that the images all appear in `<img>` tags if we
scroll through the entire page. Using a browser extension or the console, we can
simply gather all images that fall under the element
`document.querySelector("#viewerPc > div.inner.inner-delivery-contents >
div.lazy-container")`.

This method is simpler to achieve, but does require some manual input.
