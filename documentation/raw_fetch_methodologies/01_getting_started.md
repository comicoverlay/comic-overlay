# Introduction

## What this document is for

This document will not teach you the programming skills required for this task.
This document aims to provide a quick tutorial on what resources are available
for Chrome users looking to reverse-engineer the steps taken to fetch human
readable images.

## Where to find what to find

Remember that all images must come from some source your browser has access to,
whether it be stored as an image or binary data.

Open up the developer console in Chrome with `Ctrl + Shift + J`. 

It's easiest to find where images come from by looking at two of the panels from
there: `Sources` and `Network`.

### Sources

Sources contains all resource files. Most of these are scripts that come from
advertisement or tracking websites. Some of these are the images you are looking
for. This tab will always contain the code to handle DRM or unshuffling the
data to make it human viewable.

As a first step, it's usually good to look at this to see if you can find any
shuffled images in any of the `cdn` sources. Often times the shuffled image
sources will be found here. Sometimes, it's just a matter of finding out how 
those images were downloaded. If you've done the once-over and can't find it,
continue the `Network` tab.

### Network

This tab tells you everything you need to know about how a resource managed to
appear in your browser. If you refresh the page with this tab open, you will
get a rundown of when each resource was loaded and where it came from.

Click on a particular resource in the left panel and go to the initiator
sub-tab. This will show you the exact stack trace and line of code responsible
for requesting this file. From there, you can follow the code to determine what
from the HTML data triggered the download, and you can try to piece together the
javascript's actions. Note that the `Headers` sub-tab also gives information on
what request was sent and received when gathering the data.

## Advice

### Find out how other websites do it

Most websites use some altered form of another website's DRM or image shuffling.
Read through some of the documents here to get a basis of what other websites
are doing. You may find that the website's handling of the images is completely
identical to something that already exists.

Recommended order:

1. [MangaEleven](./manga_eleven.md)
2. [Web Ace](./web_ace.md)
3. [Ichijinsha](./ichijinsha.md)
4. [Comic Border](./comic_border.md)
5. [Comic Earthstar](./comic_earthstar.md)