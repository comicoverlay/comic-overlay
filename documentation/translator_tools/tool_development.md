# Developing Translator Tools

Fan translators don't want to learn how to be a developer. The goal is to make
the packaging and use of the project as simple as possible for end-users.

## Fetch Raws

### Windows
 
To build this project, open up a terminal and run:

```shell
pip install pyinstaller
# Make sure your working directory is the repo root. Then run:
pyinstaller --onefile py_src/fetch_raws.py
```
This will create the file `dist/fetch_raws.exe`. From here, you can run the
program using only that executable for most Windows compatible devices. This is
the executable we should be re-distributing for Windows users.

### Linux/Mac

TODO: Document this.

## Image Diff

TODO: Document this.