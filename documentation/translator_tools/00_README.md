# Translator Tools

This directory contains the tools used by fan translators to create the image
diffs.

These tools do NOT go over how to use Photoshop, Paint Tool SAI, GIMP, or other
image editors to clean and typeset manga. There are many other resources for
doing so, including the [Scanlator School Discord](https://discord.gg/8zpF4PE).

## Image Diffing Tool

TODO: Add this.

## Raw Downloader Tool

Run the file. You should see a terminal show up. The first time the program runs
takes a while, so be patient.

You should see a really old-looking Dialog box that
asks you for a URL. Paste the full chapter URL into the dialog box and press
"OK".

Then, select a directory you wish to download the chapter to, and click "Select
Folder" . Note that a sub-directory will be created for this specific chapter,
so you should select a folder that corresponds to the series you are trying to
download a chapter from.

There are a limited range of websites this tool is currently capable of
downloading images for. For a full list of websites, see the main
[README](../../README.md#supported-websites) file.