# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import re
import sys

import easygui

from py_src import utils
from py_src.raw_sources.comic_border import ComicBorderReader
from py_src.raw_sources.mangadex import MangadexReader
from py_src.raw_sources.web_ace import WebAceReader

# TODO: Soon. Need to add the browser extensions for these.
# from py_src.raw_sources.publus import PublusReader, ShufflessPublusReader
# from py_src.raw_sources.manga_eleven import MangaElevenReader
# from py_src.raw_sources.ichijinsha import IchijinshaReader

READERS = [
    ('.*comicborder.com/.*', ComicBorderReader),
    ('.*kuragebunch.com/.*', ComicBorderReader),
    ('.*magcomi.com/.*', ComicBorderReader),
    ('.*pocket.shonenmagazine.com/.*', ComicBorderReader),
    ('.*mangadex.org/.*', MangadexReader),

    # ('.*comic-earthstar.jp/.*', PublusReader),
    # ('.*comic-fuz.com/.*', PublusReader),
    # ('.*bookwalker.jp/.*', ShufflessPublusReader),
    ('.*web-ace.jp/.*', WebAceReader),
    # ('.*ichijinsha.co.jp/.*', IchijinshaReader),
    # ('.*manga11.com/.*', MangaElevenReader),
]


def mass_fetch(chapter_url, out_dir):
    """Fetches all chapters from a given series.

    Args:
        chapter_url: A chapter in the series. Can be any chapters.
        out_dir: The out directory for the series.
    """
    chapter_urls = ComicBorderReader().get_all_chapter_urls(chapter_url)
    for chapter_url in chapter_urls:
        get_reader(chapter_url)().solve(chapter_url, out_dir)


def get_reader(url):
    for regex, reader in READERS:
        if re.match(regex, url):
            return reader
    else:
        raise ValueError('Unable to find a suitable raw reader for URL %r', url)


def main():
    logging.basicConfig(level=logging.INFO)
    if len(sys.argv) <= 1:
        result = easygui.multenterbox(
            'Input the URL of the chapter to download.',
            'Input the URL of the chapter to download.',
            ['URL'])
        # Can be None if canceled is pressed.
        if not result:
            # Set so static analysis doesn't complain.
            url = ''
            exit(1)
        else:
            url = result[0]
    else:
        url = sys.argv[1]

    if len(sys.argv) <= 2:
        logging.getLogger('output_dir').info(
            'Please select the directory you wish to save the chapter to.')
        with utils.get_user_data_file('w+') as f:
            f.seek(0)
            user_data = json.loads(''.join(f.readlines()) or '{}')
            previous_dir = user_data.get('previous_dir', None)
            out_dir = easygui.diropenbox('Select a folder to save to.',
                                         previous_dir)
            if out_dir is None:
                exit(1)
            user_data['previous_dir'] = out_dir
        json.dump(user_data, utils.get_user_data_file('w'))
    else:
        out_dir = sys.argv[0]

    get_reader(url)().solve(url, out_dir)

    logging.info('Successfully downloaded %s to %s', url, out_dir)


if __name__ == '__main__':
    main()
