# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This script finds the difference between two images, and saves a cut-out of
the differences the translated image applies to the original.

This script helps create image diffs between two directories en-masse, provided
there are no additional images within those directories, and the images are in
the same order.
"""
import logging
import os
import re
import sys

import cv2
import easygui
import numpy as np

from py_src.img_tooling import overlay


def get_image_files(path):
    """Returns a list of all images flies in the immediate path (no recursion).
    """
    filenames = []
    for filename in next(os.walk(path))[2]:
        if filename.endswith('.png') or re.search(r'\.jpe?g$', filename):
            filenames.append(filename)
    return filenames


def get_directories(path):
    """Returns a list of all directories in the immediate path (no recursion).
    """
    return next(os.walk(path))[1]


def mask_all(raw_dir, scanlation_dir, out_dir, dilate=8):
    """Creates an overlay image for every pair of images between the raw and
    scan directories.

    Args:
        raw_dir: The directory containing ONLY the raw images. These MUST be
            exact copies from the source website. All images files within this
            directory must be PNGs or JPG/JPEGs.
        scanlation_dir: The directory containing the scans/translated copies.
            All image files within this directory must be PNGs or JPG/JPEGs.
            Raws and scans are paired up by alphabetical order within their
            respective directories; the first image to show up in the raw
            directory will be compared against the first image to show up in the
            scan directory.
        out_dir: The directory you wish to save the overlays into. The filenames
            of these overlays will match the file names of the scan images used
            to generate them. These files are saved as PNGs to preserve the
            alpha layer.
        dilate: The amount of extra pixels to leave as buffer room around the
            diff created. For scans made from screenshots or compressed images,
            this value may be higher. From translations made from the exact
            raws, this value can be lower.
    """
    for raw_fname, scan_fname in zip(get_image_files(raw_dir),
                                     get_image_files(scanlation_dir)):
        raw_img = cv2.imread(os.path.join(raw_dir, raw_fname))
        scan_img = cv2.imread(os.path.join(scanlation_dir, scan_fname))

        overlay_img = create_overlay(raw_img, scan_img, dilate)
        out_path = os.path.join(out_dir,
                                re.sub(r'\.jpe?g$', '.png', scan_fname))
        cv2.imwrite(out_path, overlay_img)
        logging.info('Successfully saved "%s"', out_path)


def create_overlay(raw_img, scan_img, dilate_size=8, dilate_iterations=1):
    """Creates an overlay image from the given raw and scan.

    Args:
        raw_img: The image data for the raw image. Must be stored in a numpy
            array.
        scan_img: The image data for the translated image. Must be stored in a
            numpy array.
        dilate_size: The size of the dilation. Dilation makes the mask pick up
            more pixels around the translated words, allowing things like text
            effects (e.g., white outlines) to be picked up, in case the
            algorithm had difficulty them up.
            The more different the scan_img is from the raw_img, the higher this
            value may need to be.
        dilate_iterations: The number of times to apply the dilation kernel.

    Returns:
        The image data for the overlay, as a numpy array.
    """
    # First, we need the scan image to be positionally aligned with the raw,
    # and have the same width and height.
    # If a scanlator took a screenshot, or merged two images together into a
    # spread, this will get around that.
    scale_matrix, aligned_scan = overlay.align_scan_to_raw(raw_img, scan_img)

    # Here, we move everything to grayscale to allow image binarization
    # algorithms to run.
    raw_gray = cv2.cvtColor(raw_img, cv2.COLOR_RGB2GRAY)
    scan_gray = cv2.cvtColor(aligned_scan, cv2.COLOR_RGB2GRAY)

    # Here, we apply a gaussian blur because if an image is JPEG'd so badly, the
    # artifacting will cause large portions to pass our binarization filtering.
    # We account for how much we scaled up the scan image to match the raw, then
    # round down to the nearest odd number.
    blur_kernel = (int((5 * max(1, scale_matrix[0, 0]) - 1) / 2) * 2 + 1,
                   int((5 * max(1, scale_matrix[1, 1]) - 1) / 2) * 2 + 1)
    raw_gray = cv2.GaussianBlur(raw_gray, blur_kernel, 0)
    scan_gray = cv2.GaussianBlur(scan_gray, (5, 5), 0)

    # TODO: A lot of scanlators like to level their images such that the darkest
    # pixel is 0, and the brightest pixel is 255. We may need to add level
    # handling here to account for this.

    # Here, we take the absolute diff between the images to find the areas where
    # the images differ the most.
    diff = cv2.absdiff(raw_gray, scan_gray)
    # We then apply Otsu's algorithm for determining the binarization threshold.
    # This will ignore some of the noise caused by scanlators resizing the
    # image, or some other form of quality loss (screenshotting, saving as JPEG,
    # etc).
    mask = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    output_img = np.zeros([*mask.shape, 4])

    # Here, we dilate the mask to pick up any nearby pixels. This is especially
    # Useful when the white border around words is not picked up by the diff.
    dilate_kernel = np.ones((dilate_size, dilate_size), np.uint8)
    mask = cv2.add(mask, cv2.dilate(mask, dilate_kernel,
                                    iterations=dilate_iterations))
    output_img[:, :, 3] = mask

    # Clears the BGR data within the transparent pixels. This greatly reduces
    # the file size and prevents copyrighted data from being stored within the
    # images.
    clip_array = np.clip(mask, 0, 1)
    output_img[:, :, 0] = aligned_scan[:, :, 0] * clip_array
    output_img[:, :, 1] = aligned_scan[:, :, 1] * clip_array
    output_img[:, :, 2] = aligned_scan[:, :, 2] * clip_array

    return output_img


def create_overlay_from_paths(raw_img_path, scan_img_path,
                              out_path='result.png',
                              dilate=8):
    """Diffs a single image, given file paths instead of images."""
    cv2.imwrite(out_path, create_overlay(
        cv2.imread(raw_img_path),
        cv2.imread(scan_img_path),
        dilate_size=dilate
    ))


def diff_entire_series(raw_directory, scan_directory, out_directory):
    """Applies mask_all to each of the immediate subdirectories within the given
    directories."""
    for raw_dir, scan_dir in zip(get_directories(raw_directory),
                                 get_directories(scan_directory)):
        out_dir = os.path.join(out_directory, os.path.basename(scan_dir))
        os.makedirs(out_dir, exist_ok=True)
        mask_all(
            os.path.join(raw_dir),
            os.path.join(scan_dir),
            out_dir
        )


def main():
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) <= 1:
        raw_dir = easygui.diropenbox('Select the folder containing the raws.')
        # Can be None if canceled is pressed.
        if not raw_dir:
            exit(1)
    else:
        raw_dir = sys.argv[1]
    logging.getLogger('input').info('Raw directory: %s', raw_dir)

    if len(sys.argv) <= 2:
        scan_dir = easygui.diropenbox('Select the folder containing the scans.')
        # Can be None if canceled is pressed.
        if not scan_dir:
            exit(1)
    else:
        scan_dir = sys.argv[1]
    logging.getLogger('input').info('Scan directory: %s', scan_dir)

    if len(sys.argv) <= 3:
        out_dir = easygui.diropenbox('Select the folder to save to (cannot be '
                                     'the raw or scan dir).')
        # Can be None if canceled is pressed.
        if not out_dir:
            exit(1)
    else:
        out_dir = sys.argv[2]
    logging.getLogger('input').info('Out directory: %s', out_dir)

    if raw_dir == out_dir or scan_dir == out_dir:
        easygui.exceptionbox('The output directory must be different from '
                             'the raw and scan directories.')
        exit(1)
    mask_all(raw_dir, scan_dir, out_dir)


if __name__ == '__main__':
    main()
