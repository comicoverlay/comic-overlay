# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import numpy as np
from PIL import Image

"""
Code adapted from the following tutorial:
    https://medium.com/analytics-vidhya/image-stitching-with-opencv-and-python-1ebd9e0a6d78
"""


def pil_to_cv2(pil_img):
    """Adapted from https://stackoverflow.com/a/14140796."""
    open_cv_image = np.array(pil_img.convert('RGB'))
    return open_cv_image[:, :, ::-1].copy()


def cv2_to_pil(cv2_img):
    """Adapted from https://stackoverflow.com/a/43234001."""
    return Image.fromarray(cv2.cvtColor(cv2_img, cv2.COLOR_BGR2RGB))


def align_scan_to_raw(raw_img, scan_img):
    """Aligns the scan image to the raw.

    Args:
        raw_img: The raw image, in BGR.
        scan_img: The scan image to align, in BGR.

    Returns:
        The matrix scan_img was transformed with to align with the raw_img.
        Returns the scan image aligned with the raw.
    """
    raw_gray = cv2.cvtColor(raw_img, cv2.COLOR_BGR2GRAY)
    scan_rgba = cv2.cvtColor(scan_img, cv2.COLOR_BGR2BGRA)
    scan_gray = cv2.cvtColor(scan_img, cv2.COLOR_BGR2GRAY)

    sift = cv2.xfeatures2d.SIFT_create()

    # find the key points and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(scan_gray, None)
    kp2, des2 = sift.detectAndCompute(raw_gray, None)

    match = cv2.BFMatcher()
    matches = match.knnMatch(des1, des2, k=2)

    # We know there should be a match, so we scale the distance factor until we
    # find the best matching values.
    #
    # I have no idea how accurate this is, I haven't bothered to dive into how
    # to find good matches just yet.
    counter = 0
    good = []
    min_match_count = 10
    if len(matches) < min_match_count:
        raise ValueError('Unable to find any matches.')
    while len(good) < min_match_count:
        factor = .03 * 2 ** counter
        good = []
        for m, n in matches:
            if m.distance < factor * n.distance:
                good.append(m)
        counter += 1

    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    matrix, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

    dst = cv2.warpPerspective(scan_rgba, matrix,
                              (raw_img.shape[1], raw_img.shape[0]))

    ret_img = Image.fromarray(raw_img)
    ret_img.paste(Image.fromarray(dst), mask=Image.fromarray(dst))
    return matrix, pil_to_cv2(ret_img)
