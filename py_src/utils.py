# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import urllib.request

WINDOWS_ILLEGAL_PATH_CHARS = ['\x7F', '"', '*', '/', ':', '<', '>', '?', '\\',
                              '|']


def sanitize_path(path_name):
    """Returns the given path, with any non-NTFS compatible characters removed.

    NTFS is more restrictive than UNIX and other OS file systems, so sanitizing
    for NTFS paths should probably cover the rest.
    """
    out_path = ''
    for char in path_name:
        if (ord(char) in range(0x00, 0x1F) or
                char in WINDOWS_ILLEGAL_PATH_CHARS):
            continue
        out_path += char
    return out_path


def get_user_data_file(mode):
    return open(os.path.join(os.path.expanduser('~'), '.raw_fetch_preferences'),
                mode)


def url_read(*args, **kwargs):
    """Reads data back from a URL using urllib.request.

    This function mocks being a browser, which gets around some 403s from some
    websites.

    Args:
        args: args to send to urllib.request.Request. Usually just the URL as a
            string.
        kwargs: kwargs to send to urllib.request.Request. Optional.
    """
    kwargs['headers'] = kwargs.get('headers', {})
    kwargs['headers']['user-agent'] = (
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
        '(KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'
    )
    return urllib.request.urlopen(
        urllib.request.Request(*args, **kwargs)
    ).read()
