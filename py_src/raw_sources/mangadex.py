# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import posixpath
import re
import urllib.parse
from distutils.version import LooseVersion
from io import BytesIO

from PIL import Image

from py_src import utils


class MangadexReader(object):
    """Okay, so this one isn't technically a raw source.

    Ex:
        https://mangadex.org/chapter/853337
    """

    # Requires a chapter ID.
    CHAPTER_API = 'https://mangadex.org/api/?id=%s&server=null&type=chapter'

    # Requires a manga ID.
    MANGA_API = 'https://mangadex.org/api/?id=%s&type=manga'

    def solve(self, url, output_dir):
        chapter_data = self.get_chapter_data(url)

        output_dir = os.path.join(url, output_dir, chapter_data['chapter'])
        os.makedirs(output_dir, exist_ok=True)

        chapter_hash = chapter_data['hash']
        server = chapter_data['server']
        for index, page_name in enumerate(chapter_data['page_array']):
            img_data = utils.url_read(
                posixpath.join(server, chapter_hash, page_name))
            extension = re.search('(\..*?)$', page_name).group(1)
            out_path = os.path.join(output_dir, str(index).zfill(2) + extension)
            Image.open(BytesIO(img_data)).save(out_path)
            logging.getLogger('create_file').info(
                'Successfully saved "%s"', out_path)

    def get_chapter_data(self, chapter_url):
        """Returns the Mangadex chapter data from their API endpoint."""
        chapter_url = urllib.parse.urlparse(chapter_url)
        chapter_id = re.search('chapter/(\d+)', chapter_url.path).group(1)

        return json.loads(
            utils.url_read(MangadexReader.CHAPTER_API % chapter_id))

    def get_manga_data(self, manga_id):
        """Returns the Mangadex manga data from their API endpoint."""
        return json.loads(utils.url_read(MangadexReader.MANGA_API % manga_id))

    def get_all_chapters(self, chapter_url):
        """Returns a list of all relevant chapter URLs (as strings).

        The order returned is the order of chapter number/id.

        Relevant chapters are determined in the following way:
            1. The same language as the given chapter URL.
            2. For each chapter specified, the same group. If the group has
               not uploaded a chapter matching the chapter name, pick another
               group from the same language.

        Note: This func requires a chapter URL because it makes the API cleaner.
        We can probably do this from a manga/series URL instead, but the
        functionality here makes it more extensible.
        """
        master_chapter_data = self.get_chapter_data(chapter_url)
        manga_data = self.get_manga_data(master_chapter_data['manga_id'])

        def get_groups_from_chapter(chapter_json):
            return {chapter_json['group_id'], chapter_json['group_id_2'],
                    chapter_json['group_id_3']}

        # Note: very unlikely to occur, but we should remove chapters if the
        # chapter was uploaded by the same group twice (e.g., a chapter was
        # uploaded where "group_id_2" matches another "group_id"). This
        # implementation picks the second one to be found, which may be
        # sub-optimal, as we'd ideally want to prioritize the chapter where
        # "group_id" matches, or is a higher ranked group_id
        # (i.e., group_id_3 < group_id_2 < group_id).
        chosen_chapters = {
            chapter_json['chapter']: chapter_id
            for chapter_id, chapter_json in manga_data['chapter'].items()
            if chapter_json['lang_code'] == master_chapter_data['lang_code'] and
               get_groups_from_chapter(chapter_json).intersection(
                   get_groups_from_chapter(master_chapter_data))
        }

        for chapter_id, chapter_json in manga_data['chapter'].items():
            if chapter_json['lang_code'] != master_chapter_data['lang_code']:
                continue
            if chapter_json['chapter'] in chosen_chapters:
                continue
            # We've found a chapter that is not covered by the groups or
            # previously discovered chapters. Add it to the relevant chapters
            chosen_chapters[chapter_json['chapter']] = chapter_id

        return [
            'https://mangadex.org/chapter/%s' % chapter_id
            for _, chapter_id in
            sorted(chosen_chapters.items(), key=lambda x: LooseVersion(x[0]))
        ]
