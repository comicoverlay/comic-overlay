# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import math
import os
import urllib.request
import urllib.parse
from PIL import Image
from bs4 import BeautifulSoup
from io import BytesIO

from py_src import utils


class ComicBorderReader(object):
    """Pulls images from ComicBorder.

    I happened on this website first, so the viewer is named after ComicBorder.
    Works for other websites as well, see below.

    #TODO: Update the class and file name to the name of the actual viewer.

    Ex:
        https://comicborder.com/episode/10834108156644502252
        https://kuragebunch.com/episode/10834108156663037088
        https://pocket.shonenmagazine.com/episode/10834108156694419843
        https://magcomi.com/episode/10834108156766387850
    """

    def __init__(self):
        pass

    def get_episode_json(self, url):
        soup = BeautifulSoup(utils.url_read(url), features='html.parser')
        return json.loads(
            soup.find('script', {'id': 'episode-json'})['data-value'])

    def solve(self, url, output_dir):
        episode_json = self.get_episode_json(url)

        # Some readers prevent some chapters from being readable, by placing
        # the chapter as a private episode.
        # We shouldn't be trying to circumvent the paywall. Raise an error.
        if ('pageStructure' not in episode_json['readableProduct'] or
                not episode_json['readableProduct']['pageStructure']):
            logging.error('Chapter %s is a private chapter. Skipping.', url)
            return

        base_dir = os.path.join(output_dir,
                                episode_json['readableProduct']['title'])
        os.makedirs(base_dir, exist_ok=True)

        all_pages = episode_json['readableProduct']['pageStructure']['pages']
        cho_ju_giga = episode_json['readableProduct']['pageStructure'][
            'choJuGiga']
        for index, page in enumerate(all_pages):
            if 'src' not in page:
                continue
            if cho_ju_giga == 'baku':
                solve_image = self._solve_image_baku
            else:
                solve_image = self._solve_image_other
            img = solve_image(page['src'], page['width'], page['height'])
            out_path = os.path.join(base_dir, str(index).zfill(2) + '.png')
            img.save(out_path)
            logging.getLogger('create_file').info(
                'Successfully saved "%s"', out_path)

    def _solve_image_other(self, image_url, *_):
        """So far, we only see 'usagi' as an alternative to baku.

        If baku is not present, pocket.shonenmagazine skips the DRM unshuffle.
        Search the source JS for 'skipDrm' or 'baku'.
        """
        return Image.open(BytesIO(utils.url_read(image_url)))

    def _solve_image_baku(self, image_url, width, height):
        img = Image.open(BytesIO(utils.url_read(image_url)))

        divide_num = 4
        multiple = 8
        cell_width = math.floor(width / (divide_num * multiple)) * multiple
        cell_height = math.floor(height / (divide_num * multiple)) * multiple

        final_img = img.copy()
        e = 0
        while e < divide_num * divide_num:
            t = math.floor(e / divide_num) * cell_height
            n = e % divide_num * cell_width
            r = math.floor(e / divide_num)
            i = e % divide_num * divide_num + r
            s = i % divide_num * cell_width
            o = math.floor(i / divide_num) * cell_height
            final_img.paste(img.crop((n, t, n + cell_width, t + cell_height)),
                            (s, o))
            e += 1
        return final_img

    def get_all_chapter_urls(self, chapter_url):
        """Returns the list of all URLs as strings from a given chapter URL."""
        response = urllib.request.urlopen(chapter_url)
        html_data = response.read()
        soup = BeautifulSoup(html_data, features='html.parser')

        query = {'class': 'js-readable-product-list'}
        series_api_url = soup.find('div', query)['data-latest-list-endpoint']
        # Found in the chunk.js files
        parsed_url = urllib.parse.urlparse(series_api_url)
        api_query = urllib.parse.parse_qs(parsed_url.query)
        api_query['number_until'] = 0
        parsed_api_url = parsed_url._replace(
            query=urllib.parse.urlencode(api_query, doseq=True))
        api_url = urllib.parse.urlunparse(parsed_api_url)

        json_string = utils.url_read(api_url).decode('utf-8', errors='replace')
        soup = BeautifulSoup(json.loads(json_string)['html'],
                             features='html.parser')

        # Hack. Injects the href to make the next line a one-liner.
        cur_li = soup.find('li', {'class': 'current-readable-product'})
        cur_li.contents[1].name = 'a'
        cur_li.contents[1]['href'] = chapter_url

        return [a['href'] for a in reversed(soup.find_all('a'))]
