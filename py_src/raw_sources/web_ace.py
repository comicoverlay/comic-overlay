# Copyright 2020 Comic Overlay Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import urllib.parse
import urllib.request
from io import BytesIO

import requests
from PIL import Image
from bs4 import BeautifulSoup

from py_src import utils


class WebAceReader(object):
    """Pulls images from web-ace.jp.

    Ex:
        https://web-ace.jp/youngaceup/contents/1000137/episode/4262/
    """

    def __init__(self):
        pass

    def solve(self, url, output_dir):
        parsed_url = urllib.parse.urlparse(url)

        # Fetch the web page
        soup = BeautifulSoup(utils.url_read(url),
                             features='html.parser')

        title = soup.find('title').contents[0].strip()

        json_url_path = (
            soup.find('div', {'class': 'lazy-container'})['data-url'])

        # Apparently using the _replace is the documented way...
        # https://docs.python.org/3/library/urllib.parse.html#urllib.parse.urlparse
        json_url = parsed_url._replace(path=json_url_path)

        response = urllib.request.urlopen(json_url.geturl())
        html_data = response.read()
        image_urls = json.loads(html_data.decode('utf-8'))

        base_dir = os.path.join(output_dir, utils.sanitize_path(title))
        os.makedirs(base_dir, exist_ok=True)

        for img_num, img_url in enumerate(image_urls):
            img_url = parsed_url._replace(path=img_url)
            response = requests.get(img_url.geturl())
            out_path = os.path.join(base_dir, str(img_num).zfill(2)) + '.png'
            Image.open(BytesIO(response.content)).save(out_path)
            logging.getLogger('create_file').info(
                'Successfully saved "%s"', out_path)
