// https://comicborder.com/episode/10834108156644502252?1=BQ09wbB&2=OVlAGUa&3=oAqx0R0&4=ffcLFQz&5=ex86nJZ&6=SJMJEJC&7=CxuXFKY&8=H8bMvOR&9=BFnLfzj&10=7qfjKAB&11=7XDNhoj&12=SA2AmaU&13=JOuLBoo&14=fSyrpRG&15=uzpWm2m&16=eKowe04&17=7cpOgrq&18=9WiLCil
// https://kuragebunch.com/episode/10834108156705952451?1=HfVtDQh&2=XHsf6xY&3=XOKz4cC&4=EpMAd01&5=IvlNCwl&6=PZXcunL

// canvas_id -> seen count. To be safe, a seen count of 2 is required for updating the image.
var seen_count = {};
var seen_canvases = [];

episodes = {
    // 1                     "CyTzueKg","gkce9d9g","p61ApPPg","Gv5eGPXg","8USsmUFg","U8RzBDRg","8egm9fEg","RZafeKrg"
    "10834108156655033464": ["sjMqfJ4/01", "WxpVk3w/02", "VYJpWpS/03", "tP8X12D/04", "vVQJ4PB/05", "6y7mvLB/06", "yNMmCFn/07", "Df1TpN0/08"],
    // 3
    "10834108156655033470": ["Cv1RJ9R/01", "RztcYkP/02", "xhCbgnf/03", "MRvQwbp/04", "3rbZYpP/05", "KLgz4hs/06", "sH5YQCc/07"],
    // 4
    "10834108156655033473": ["nR6my4D/01", "484WpqL/02", "S7TprBx/03", "4SJ3jrM/04", "SXS5rwQ/05", "syyGGqX/06"],
    // 8
    "10834108156663037088": ["h2xSmW0/01", "pX0PpNR/02", "R4qPYTj/03", "jH04gTt/04", "10fDtt5/05", "znS9mQW/06"],
    // 9
    "10834108156663037091": ["J5fPyvf/01", "ZSqNmFM/02", "zbLT844/03", "f2RRCt2/04", "wKHH5t9/05", "XXRR7N7/06", "9YJsXFM/07", "crdNFbn/08"],
    // 13
    "10834108156663037103": ["8bNrQQk/01", "kcqBzhX/02", "WFfwxZG/03", "n6ztPJy/04", "WKkkV3c/05", "wcyrxZn/06"],
    // 17
    "10834108156663037115": ["Gkt7Qwz/00", "nCL9M7g/01", "wWbRzLf/02", "KcP1Z7R/03", "8gBWN7r/04", "j5H1Pjy/05", "nfzWVyX/06", "ZMyxNw5/07"],
    // 19
    "10834108156663037121": ["Tm6SKbJ/01", "zbm2Vw3/02", "2WY70zH/03", "RPmK5Nd/04", "SN6r5Bv/05", "PTNvKfp/06", "KsVD5vd/07", "6XRfBJp/08"],
    // 20
    "10834108156663037237": ["vPcfydR/01", "NjHLpnB/02", "jRxNG5k/03", "h19qGxw/04", "V25dqyw/05", "s1YmZbH/06"],
    // 21
    "10834108156689698017": ["zV1fJd6/01", "9rzSfJ9/02", "bPGfsjk/03", "17TmxxK/04", "QQbsKPs/05", "7j3p7fT/06", "0DLbP1F/07", "Pw9s4h5/08"],
    // 22
    "10834108156689698020": ["FK7stmt/01", "dGnMLQ3/02", "x6HN8kj/03", "VT7D6bR/04", "Czv4BLJ/05", "LtPddhK/06"],
    // 23
    "10834108156689698023": ["DrZ49Hs/01", "MM3SbbB/02", "stZjMHY/03", "5WqqSCv/04", "4NpP7tk/05", "WvbDjh6/06"],
    // 24
    "10834108156700923337": ["p4Vg7ZZ/01", "wpHCfdX/02", "FVchrhY/03", "mt7knCn/04", "d0xmC24/05", "7J7cV2W/06", "nMCfBZN/07", "z6kwGNM/08", "KX33vVP/09", "56H9VMM/10"],
    // 25
    "10834108156700923421": ["R2GskvV/01", "7v1H5cD/02", "M59W6TW/03", "fd55P1H/04", "nDz8Kt8/05", "m90CHmD/06"],
    // 26
    "10834108156705952410": ["BjrrpPf/01", "Mkzn6S0/02", "554zNG8/03", "khd8Wsj/04", "wRCh96x/05", "3zxyjww/06", "tbVtRg8/07", "KL2C0tr/08"],
    // 27
    "10834108156705952413": ["vP1018b/01", "gV5ss9B/02", "h98yM01/03", "HxHxcdr/04", "6WJtmSd/05", "5BppJTm/06", "P6JLc9Q/07", "TRRy84w/08"],
    // 28
    "10834108156705952416": ["zZnNVpP/01", "RSLh5Qg/02", "Q8dmKjd/03", "RgbXDwk/04", "4YLP1Tq/05", "yR1K4Ww/06"],
    // 29
    "10834108156705952419": ["jM0J4fB/01", "BVQsnW3/02", "944xBgs/03", "C1Zst1J/04", "NWzZpgg/05", "drvt9sf/06", "DWWRf9M/07", "C8rKcL4/08"],
    // 30
    "10834108156705952424": ["WfhQJ37/01", "Bwhjf2h/02", "d0b2MyJ/03", "4mjTkHP/04", "B48PG5C/05", "6yMk8kS/06"],
    // 31
    "10834108156705952427": ["pvvkwwf/01", "LSrxTMB/02", "WgZgxKx/03", "8gs8hbH/04", "MSZJNZL/05", "4F2qq6h/06"],
    // 32
    "10834108156705953333": ["wwJ2kLK/01", "hd4xys4/02", "BrmnYM2/03", "JsqpHbn/04", "71sfJNF/05", "BwNG4Cd/06"],
    // 33
    "10834108156705952430": ["NTnhpWf/01", "5crx27Y/02", "Mf0Mr2L/03", "TLSTwhP/04", "Jv2JXBB/05", "q7vBJ26/06"],
    // 34
    "10834108156705952433": ["9vc81cQ/01", "Gk9VbNk/02", "NKw4nTD/03", "MVRFXdS/04", "N18v27q/05", "PMcrYTJ/06", "FnhyF0z/07", "CvZzCFX/08"],
    // 35
    "10834108156705952436": ["cymZgG9/01", "D813kvy/02", "NyfK4hw/03", "b3p4FF6/04", "Wn082d9/05", "tm1rL8r/06"],
    // 36
    "10834108156705952439": ["cbSw36m/01", "1XPLJQ2/02", "LnYHgmF/03", "mXhwqzR/04", "MCq1KSZ/05", "3WDNtCB/06", "ZGKk2H2/07", "myYzNZM/08"],
    // 37
    "10834108156705952442": ["ZxGThbC/01", "Rp4f595/02", "2nJW20c/03", "7pr2j14/04", "3StcBtc/05", "MMLcNSF/06", "qxrvC3R/07", "qRW8yzJ/08"],
    // 38
    "10834108156705952445": ["3NXZsdJ/01", "JmTrpPv/02", "hdrT99g/03", "sg2p9mL/04", "GMmtJ91/05", "r2gkHZ7/06", "4JNK78V/07", "hV852X2/08", "QYfGFPT/09", "bFbF8Vx/10"],
    // 39
    "10834108156705952448": ["g4tDp2M/01", "PC00Nm0/02", "MS6v6HL/03", "VpZmB5M/04", "9wDv2HL/05", "k16mvkV/06"],
    // 40
    "10834108156705952451": ["0YWwhXv/01", "YXLLnBh/02", "H2vKxPY/03", "XV6wHZC/04", "Fx8fLpc/05", "KLHQXfd/06"],
    // 41
    "10834108156705952454": ["Qm83xkc/01", "VQYhhMs/02", "zrJZPwr/03", "6bpMMTK/04", "gTkRzT8/05", "r5Q8scq/06"],
    // 42
    "10834108156705952457": ["hM9GmyV/01", "1nSDsvh/02", "1Qkc8Cp/03", "ysFJZv9/04", "vj4CQDT/05", "DwwhzBg/06", "pypBJyf/07", "PNLb0XJ/08"],
    // 43
    "10834108156762993393": ["HHQLBRS/01", "GFRJ2Sz/02", "HpWqwnb/03", "ccDj8Kp/04", "61hfJCT/05", "w4Nd6r8/06"],
    // 44
    "10834108156762993398": ["93DQmWj/01", "VWh5HTj/02", "HxkfPyw/03", "dLqhPXV/04", "rtMMpTm/05", "cxwc98w/06", "8s1tY4J/07", "mXYG7MJ/08"],
    // 45
    "10834108156762993401": ["LZBDqRV/01", "nMFYC9p/02", "PFJsp2Z/03", "cgcSkbg/04", "xf6tp1X/05", "m6JLZqK/06", "P6h8wQ2/07", "37VHLSX/08", "cJkH14F/09", "T2S0brJ/10"],
    // 46
    "13933686331614294618": ["Y2cF3Jf/06", "3cd7xK2/05", "HC6sxgh/04", "TkxtS9L/03", "X5ZvjqP/02", "Rc15yGV/01"]
}

function makeDrawImage(canvas, img) {
    return function drawImage() {
        return canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);
    }
}

// Handles the canvas version of showing the images to the viewer
function update_canvases () {
    canvases = document.querySelectorAll("canvas");
    for (const canvas of canvases) {
        if (canvas.id == "") {
            var index = -1;
            var iterator = canvas.parentElement;
            while( (iterator = iterator.previousSibling) != null ) {index++;}
            canvas.id = index;
            seen_count[canvas.id] = 0;
            seen_canvases[Object.keys(seen_canvases).length] = canvas;
        }
        seen_count[canvas.id] += 1;
    }
    for (i = 0; i < seen_canvases.length; i++) {
        canvas = seen_canvases[i];
        if (seen_count[canvas.id] <= 2) {
            img = new Image();
            img.onload = makeDrawImage(canvas, img);
            img.src = "https://i.ibb.co/" + episodes[document.URL.match(".*/(.*?)$")[1]][canvas.id] + ".png";
        }
    }
}

// Sometimes, the reader provides images instead of canvases (usually for mobile
// browsers). When this happens, we convert the images back to canvases.
function force_use_canvases() {
    page_areas = document.querySelectorAll("p.page-area");
    for (const page_area of page_areas) {
        if (page_area.id == "") {
            var index = -1;
            var iterator = page_area;
            while( (iterator = iterator.previousSibling) != null ) {index++;}
            page_area.id = index;
        }
        if (page_area.children.length == 1 && page_area.children[0].tagName != "CANVAS") {
            img_element = page_area.children[0];
            img_element.style = 'display:none;';
            page_area.insertAdjacentHTML('beforeend',
            '<canvas width="' + img_element.naturalWidth +
            '" height="' + img_element.naturalHeight +
            '" class="page-image js-page-image"></canvas>');
            page_area.children[1].getContext("2d").drawImage(img_element, 0, 0);
        }
    }
}

function update() {
    force_use_canvases();
    update_canvases();
}

document.body.onclick = update;
document.addEventListener("DOMNodeInserted", update);
// Allows bookmarklets to instantly see the translations.
document.body.click();

