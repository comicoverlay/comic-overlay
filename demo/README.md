# Demo

Please read the main [README](../README.md) if you have not yet. This demo
contains overlays created by this project, but due to lack of content hosting
solutions, only contains the translations for a single series.

## Getting the demo

Like the end-user extension, the demo has several ways of being packaged to
users.

## Install on Chrome

[Here's a link](https://chrome.google.com/webstore/detail/ffbnpdfobbfkapdkcnhgjekanmameopi/publish-accepted?authuser=0&hl=en)
to the demo extension.

## Install on Firefox and Firefox for Android

[Here's a link](https://addons.mozilla.org/en-US/firefox/addon/comic-overlay-demo/)
to the demo extension.

## Using a bookmarklet

It is strongly recommended that, if possible, you use one of the other methods
of obtaining the project. Otherwise, Mobile and Safari users should use this
method.

This method of enabling Comic Overlays requires the user to click on the
bookmark on every newly-loaded URL. Because of this, mobile users are
recommended to name the bookmark something easy to type next to the `Enter` key
(usually something like `mmm`). For Desktop users, it is recommended to add the
bookmarklet to the bookmarks bar. This way, no typing is required, and the
bookmarklet name can be easily identifiable (e.g., naming it `English
Translations`).

You can create a bookmarklet by bookmarking a page, and then changing the URL
link to:

```
function e(b){var a=0;return function(){return a<b.length?{done:!1,value:b[a++]}:{done:!0}}}function f(b){var a="undefined"!=typeof Symbol&&Symbol.iterator&&b[Symbol.iterator];return a?a.call(b):{next:e(b)}}var g={},h=[];
episodes={"10834108156655033464":"sjMqfJ4/01 WxpVk3w/02 VYJpWpS/03 tP8X12D/04 vVQJ4PB/05 6y7mvLB/06 yNMmCFn/07 Df1TpN0/08".split(" "),"10834108156655033470":"Cv1RJ9R/01 RztcYkP/02 xhCbgnf/03 MRvQwbp/04 3rbZYpP/05 KLgz4hs/06 sH5YQCc/07".split(" "),"10834108156655033473":"nR6my4D/01 484WpqL/02 S7TprBx/03 4SJ3jrM/04 SXS5rwQ/05 syyGGqX/06".split(" "),"10834108156663037088":"h2xSmW0/01 pX0PpNR/02 R4qPYTj/03 jH04gTt/04 10fDtt5/05 znS9mQW/06".split(" "),"10834108156663037091":"J5fPyvf/01 ZSqNmFM/02 zbLT844/03 f2RRCt2/04 wKHH5t9/05 XXRR7N7/06 9YJsXFM/07 crdNFbn/08".split(" "),
"10834108156663037103":"8bNrQQk/01 kcqBzhX/02 WFfwxZG/03 n6ztPJy/04 WKkkV3c/05 wcyrxZn/06".split(" "),"10834108156663037115":"Gkt7Qwz/00 nCL9M7g/01 wWbRzLf/02 KcP1Z7R/03 8gBWN7r/04 j5H1Pjy/05 nfzWVyX/06 ZMyxNw5/07".split(" "),"10834108156663037121":"Tm6SKbJ/01 zbm2Vw3/02 2WY70zH/03 RPmK5Nd/04 SN6r5Bv/05 PTNvKfp/06 KsVD5vd/07 6XRfBJp/08".split(" "),"10834108156663037237":"vPcfydR/01 NjHLpnB/02 jRxNG5k/03 h19qGxw/04 V25dqyw/05 s1YmZbH/06".split(" "),"10834108156689698017":"zV1fJd6/01 9rzSfJ9/02 bPGfsjk/03 17TmxxK/04 QQbsKPs/05 7j3p7fT/06 0DLbP1F/07 Pw9s4h5/08".split(" "),
"10834108156689698020":"FK7stmt/01 dGnMLQ3/02 x6HN8kj/03 VT7D6bR/04 Czv4BLJ/05 LtPddhK/06".split(" "),"10834108156689698023":"DrZ49Hs/01 MM3SbbB/02 stZjMHY/03 5WqqSCv/04 4NpP7tk/05 WvbDjh6/06".split(" "),"10834108156700923337":"p4Vg7ZZ/01 wpHCfdX/02 FVchrhY/03 mt7knCn/04 d0xmC24/05 7J7cV2W/06 nMCfBZN/07 z6kwGNM/08 KX33vVP/09 56H9VMM/10".split(" "),"10834108156700923421":"R2GskvV/01 7v1H5cD/02 M59W6TW/03 fd55P1H/04 nDz8Kt8/05 m90CHmD/06".split(" "),"10834108156705952410":"BjrrpPf/01 Mkzn6S0/02 554zNG8/03 khd8Wsj/04 wRCh96x/05 3zxyjww/06 tbVtRg8/07 KL2C0tr/08".split(" "),
"10834108156705952413":"vP1018b/01 gV5ss9B/02 h98yM01/03 HxHxcdr/04 6WJtmSd/05 5BppJTm/06 P6JLc9Q/07 TRRy84w/08".split(" "),"10834108156705952416":"zZnNVpP/01 RSLh5Qg/02 Q8dmKjd/03 RgbXDwk/04 4YLP1Tq/05 yR1K4Ww/06".split(" "),"10834108156705952419":"jM0J4fB/01 BVQsnW3/02 944xBgs/03 C1Zst1J/04 NWzZpgg/05 drvt9sf/06 DWWRf9M/07 C8rKcL4/08".split(" "),"10834108156705952424":"WfhQJ37/01 Bwhjf2h/02 d0b2MyJ/03 4mjTkHP/04 B48PG5C/05 6yMk8kS/06".split(" "),"10834108156705952427":"pvvkwwf/01 LSrxTMB/02 WgZgxKx/03 8gs8hbH/04 MSZJNZL/05 4F2qq6h/06".split(" "),
"10834108156705953333":"wwJ2kLK/01 hd4xys4/02 BrmnYM2/03 JsqpHbn/04 71sfJNF/05 BwNG4Cd/06".split(" "),"10834108156705952430":"NTnhpWf/01 5crx27Y/02 Mf0Mr2L/03 TLSTwhP/04 Jv2JXBB/05 q7vBJ26/06".split(" "),"10834108156705952433":"9vc81cQ/01 Gk9VbNk/02 NKw4nTD/03 MVRFXdS/04 N18v27q/05 PMcrYTJ/06 FnhyF0z/07 CvZzCFX/08".split(" "),"10834108156705952436":"cymZgG9/01 D813kvy/02 NyfK4hw/03 b3p4FF6/04 Wn082d9/05 tm1rL8r/06".split(" "),"10834108156705952439":"cbSw36m/01 1XPLJQ2/02 LnYHgmF/03 mXhwqzR/04 MCq1KSZ/05 3WDNtCB/06 ZGKk2H2/07 myYzNZM/08".split(" "),
"10834108156705952442":"ZxGThbC/01 Rp4f595/02 2nJW20c/03 7pr2j14/04 3StcBtc/05 MMLcNSF/06 qxrvC3R/07 qRW8yzJ/08".split(" "),"10834108156705952445":"3NXZsdJ/01 JmTrpPv/02 hdrT99g/03 sg2p9mL/04 GMmtJ91/05 r2gkHZ7/06 4JNK78V/07 hV852X2/08 QYfGFPT/09 bFbF8Vx/10".split(" "),"10834108156705952448":"g4tDp2M/01 PC00Nm0/02 MS6v6HL/03 VpZmB5M/04 9wDv2HL/05 k16mvkV/06".split(" "),"10834108156705952451":"0YWwhXv/01 YXLLnBh/02 H2vKxPY/03 XV6wHZC/04 Fx8fLpc/05 KLHQXfd/06".split(" "),"10834108156705952454":"Qm83xkc/01 VQYhhMs/02 zrJZPwr/03 6bpMMTK/04 gTkRzT8/05 r5Q8scq/06".split(" "),
"10834108156705952457":"hM9GmyV/01 1nSDsvh/02 1Qkc8Cp/03 ysFJZv9/04 vj4CQDT/05 DwwhzBg/06 pypBJyf/07 PNLb0XJ/08".split(" "),"10834108156762993393":"HHQLBRS/01 GFRJ2Sz/02 HpWqwnb/03 ccDj8Kp/04 61hfJCT/05 w4Nd6r8/06".split(" "),"10834108156762993398":"93DQmWj/01 VWh5HTj/02 HxkfPyw/03 dLqhPXV/04 rtMMpTm/05 cxwc98w/06 8s1tY4J/07 mXYG7MJ/08".split(" "),"10834108156762993401":"LZBDqRV/01 nMFYC9p/02 PFJsp2Z/03 cgcSkbg/04 xf6tp1X/05 m6JLZqK/06 P6h8wQ2/07 37VHLSX/08 cJkH14F/09 T2S0brJ/10".split(" "),"13933686331614294618":"Y2cF3Jf/06 3cd7xK2/05 HC6sxgh/04 TkxtS9L/03 X5ZvjqP/02 Rc15yGV/01".split(" ")};
function k(){var b=canvas,a=img;return function(){return b.getContext("2d").drawImage(a,0,0,b.width,b.height)}}
function l(){page_areas=document.querySelectorAll("p.page-area");for(var b=f(page_areas),a=b.next();!a.done;a=b.next()){a=a.value;if(""==a.id){for(var c=-1,d=a;null!=(d=d.previousSibling);)c++;a.id=c}1==a.children.length&&"CANVAS"!=a.children[0].tagName&&(img_element=a.children[0],img_element.style="display:none;",a.insertAdjacentHTML("beforeend",'<canvas width="'+img_element.naturalWidth+'" height="'+img_element.naturalHeight+'" class="page-image js-page-image"></canvas>'),a.children[1].getContext("2d").drawImage(img_element,
0,0))}canvases=document.querySelectorAll("canvas");b=f(canvases);for(a=b.next();!a.done;a=b.next()){a=a.value;if(""==a.id){c=-1;for(d=a.parentElement;null!=(d=d.previousSibling);)c++;a.id=c;g[a.id]=0;h[Object.keys(h).length]=a}g[a.id]+=1}for(i=0;i<h.length;i++)canvas=h[i],2>=g[canvas.id]&&(img=new Image,img.onload=k(),img.src="https://i.ibb.co/"+episodes[document.URL.match(".*/(.*?)$")[1]][canvas.id]+".png")}document.body.onclick=l;document.addEventListener("DOMNodeInserted",l);document.body.click();
```


If you are having trouble please do an internet search engine query for
`<browser> create bookmarklet`. There are plenty of resources online.

<!--For project maintainers: For this demo, there will be no updates to the
script. As a result, the bookmarklet does not need to be loaded in from a URL.
This is not true for the full project, as new websites will be added, and
existing websites may need updates when the websites change.-->
