# Comic Overlay (working title)

Note that there are assumptions about legality in this document. I'm not a
lawyer, so don't take this as sound legal information. If you see something that
is legally incorrect in this document, __please__ reach out to correct it.

## Background

At the moment, many great comics are created by very busy, diligent artists. Due
to language barriers and the amount of work required to translate weekly
chapters for international release, most serialized works go completely
untranslated.

The medium itself doesn't lend well to translation. Many text and sound effects
are difficult to translate, and the work there needs to be completely duplicated
for a quality release.

Fans have took up this work themselves to translate the work for their
language/locale. The way most fan translations are done is illegal: the original
source material ("raws") are translated and then re-distributed across other
websites, breaking international copyright law. However, this is the only
existing solution to share the works with others who do not understand the
language of the original work.

## Core Vision

To stop breaking international copyright law, we need a system that allows for
the re-distribution of the translation without re-distributing the original
work.

To achieve this, we can create an overlay image to draw over the original comic
page. This overlay will contain only the information required to transform the
original work into another language. This way, copyrighted material from the
original comic is not being re-distributed by the fan translation website. Only
the translated text/redraws are being distributed.

As an example, take these panels from a manga called 「カラーレス」 (Colorless) by
KENT:

![カラーレス, Ch.3, Japanese](documentation/resources/ex_raw.png)

We can draw the following overlay image over it to translate the page to
English:

![カラーレス, Ch.3, English Overlay](documentation/resources/ex_overlay.png)

Resulting in the image:

![カラーレス, Ch.3, English](documentation/resources/ex_translated.png)

To allow readers to see these translations, we need to create a browser
extension (or [bookmarklet](https://bookmarklets.org/what-is-a-bookmarklet/))
that draws the overlays on top of the original comic pages from the licensed
distributor.

To see this in action, please check out the demo at
[demo/README.md](./demo/README.md).

## Benefits

This has two crucial benefits:

1. The overlays only contain changes to the image created by the fan translator.
   This ensures there is no copyrighted material being re-distributed.
2. The original website gets international traffic from the fans who otherwise
   cannot read the comic, which allows international readers to support the
   official release.

## Caveats

### Mobile

Mobile users are now the primary market for online comics. The largest mobile
browsers (Chrome, Safari) currently do not support extensions capable of drawing
the overlay over the original source material.

For browsers like FireFox that do support mobile extensions, an extension can be
released. For other browsers, a
[bookmarklet](https://bookmarklets.org/what-is-a-bookmarklet/) can be used. A
bookmarklet is a bookmark that runs some JavaScript, which can contain a copy of
the code that the extension would have otherwise run.

This is also a tough sell to the thousands of users of third-party comic reader
apps. There are currently no plans to support these apps, as they have no way of
supporting the official release.

### Ripping

To be able to use this method, fan translators ***must*** have a local copy of
the original pages in order to create the overlay. To obtain this file, the fan
translator must gather the image on their own device (so illegal re-distribution
does not occur).

To aid fan translators in gathering these images, this project will also contain
code to download the raws off of the original website. This is an incredibly
important step to achieving these benefits, and the only way to redirect
international readers to the original website.

The act of downloading this files is not illegal (at least in the US), but may
go against the terms of service of the website. Your browser already downloads
these files from the website's Content Delivery Network (CDN) and applies the
Digital Rights Management (DRM) unlock method so the pages can be displayed
in-browser. This project uses a headless browser to achieve the same effect.

To ensure we are doing the right thing, this project will only contain code that
downloads comics if the website is supported by the browser extension.

#### One last step for fan translators

After a fan translator is finished translating a page or chapter, they must
generate the overlay image. To do this, they can run the image diffing tool
provided in this project. Details on how to download and use this tool can be
found under
[documentation/img_tooling/README.md](./documentation/img_tooling/00_README.md).

## Project Overview

With this information in mind, the project is split into three core pieces:

1. Tools to move fan translators away from illegal re-distribution. This
   includes tooling to download raws and an image diffing tool to prevent
   copyrighted material from appearing in the overlay image.
2. A CDN to host the overlay images containing the translations on a centralized
   website. Ideally, this website should be able to support multiple locales so
   the work we do here will allow even more international traffic to be
   re-routed to the original source website.
3. A browser extension that can draw the overlay images over the original
   artwork.

This particular repository focuses on items 1 and 3 from the above list. It is
preferred that we use an existing solution for Item 2. Ideally, this would be
Mangadex, since they already do not make money off of direct web traffic and
support multiple languages/locales in the translations.

## Supported Websites

Currently, the project supports overlaying images for the following websites (in
alphabetical order by domain name):

* [comicborder.com](http://comicborder.com)
* [kuragebunch.com](http://kuragebunch.com)
* [magcomi.com](http://magcomi.com)
* [shonenmagazine.com](http://shonenmagazine.com)
* [web-ace.jp](http://web-ace.jp)

As we work on expanding the project, more websites will be added to this list.
Expect the following websites to be added soon™:

* [bookwalker.jp](http://bookwalker.jp)
* [comic-earthstar.jp](http://comic-earthstar.jp)
* [comic-fuz.com](http://comic-fuz.com)
* [ichijinsha.co.jp](http://ichijinsha.co.jp)

We take requests as well. Currently, requests for websites with more translated
works are being prioritized to have as much positive impact on the community and
industry as possible.

Please note that the website being supported does not mean the comic you want to
read is available through the browser extension just yet.
